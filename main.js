#!/usr/bin/env node

const fs = require('fs');
const path = require('path');


function sleep(ms) {
    ms = (ms) ? ms : 0;
    return new Promise(resolve => {setTimeout(resolve, ms);});
}

process.on('uncaughtException', (error) => {
    console.error(error);
    process.exit(1);
});

process.on('unhandledRejection', (reason, p) => {
    console.error(reason, p);
    process.exit(1);
});

const puppeteer = require('puppeteer');

// console.log(process.argv);

// if (!process.argv[2]) {
//     console.error('ERROR: no url arg\n');

//     console.info('for example:\n');
//     console.log('  docker run --shm-size 1G --rm -v /tmp:/screenshots \\');
//     console.log('  alekzonder/puppeteer:latest screenshot \'https://www.google.com\'\n');
//     process.exit(1);
// }

var url = 'http://yunqiang_wu.gitee.io/es6-crawler-ci-demo/';
var distDir = path.resolve(__dirname, './dist');
if (!fs.existsSync(distDir)) {
    fs.mkdirSync(distDir);
}

var now = new Date();

var dateStr = now.toISOString();

var width = 1366;
var height = 768;

// if (typeof process.argv[3] === 'string') {
//     var [width, height] = process.argv[3].split('x').map(v => parseInt(v, 10));
// }

var delay = 0;

if (typeof process.argv[4] === 'string') {
    delay = parseInt(process.argv[4], 10);
}

var isMobile = false;

let filename = `screenshot.png`;

(async() => {

    const browser = await puppeteer.launch({
        args: [
        '--no-sandbox',
        '--disable-setuid-sandbox'
        ]
    });

    const page = await browser.newPage();

    page.setViewport({
        width,
        height,
        isMobile
    });

    await page.goto(url, {waitUntil: 'networkidle2'});

    await sleep(delay);

    await page.screenshot({path: path.resolve(distDir, `./${filename}`), fullPage: false});

    browser.close();

    console.log(
        JSON.stringify({
            date: dateStr,
            timestamp: Math.floor(now.getTime() / 1000),
            filename,
            width,
            height
        })
    );

})();